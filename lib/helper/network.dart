import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';

abstract class network {
  var isNetConnected = true;

  StreamController isNetConnectedController = StreamController();

  late Stream myStream;

  network(  Connectivity().onConnectivityChanged.listen((status) {
      if (status == ConnectivityResult.none) {
        pingTest();
      }
    });

    isNetConnectedController.sink.add(isNetConnected);

    myStream = isNetConnectedController.stream.asBroadcastStream(););


}
