import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';

extension ParseToString on ConnectivityResult {
  String toValue() {
    return this.toString().split('.').last;
  }
}

class NoNetwork extends StatefulWidget {
  NoNetwork({Key? key}) : super(key: key);

  @override
  State<NoNetwork> createState() => _NoNetworkState();
}

class _NoNetworkState extends State<NoNetwork> {
  // ConnectivityResult? _connectivityResult;
  // late StreamSubscription _connectivitySubscription;
  var isNetConnected = false;

  StreamController isNetConnectedController = StreamController();

  late Stream myStream;

  // StreamController<bool> get isNetConnectedControllerrr {
  //   return isNetConnected;
  // }

  // @override
  // initState() {
  //   super.initState();

  //   _connectivitySubscription = Connectivity()
  //       .onConnectivityChanged
  //       .listen((ConnectivityResult result) {
  //     // print('Current connectivity status: $result');
  //     setState(() {
  //       _connectivityResult = result;
  //     });
  //   });
  // }

  // Future<void> _checkConnectivityState() async {
  //   final ConnectivityResult result = await Connectivity().checkConnectivity();

  //   if (result == ConnectivityResult.wifi) {
  //     print('Connected to a Wi-Fi network');
  //   } else if (result == ConnectivityResult.mobile) {
  //     print('Connected to a mobile network');
  //   } else {
  //     print('Not connected to any network');
  //   }

  //   setState(() {
  //     _connectivityResult = result;
  //   });
  // }

  // Future<void> _tryConnection() async {
  //   try {
  //     final response = await InternetAddress.lookup('google.com');

  //     setState(() {
  //       _isConnectionSuccessful = response.isNotEmpty;
  //     });
  //   } on SocketException catch (e) {
  //     print(e);
  //     setState(() {
  //       _isConnectionSuccessful = false;
  //     });
  //   }
  // }

  @override
  initState() {
    super.initState();
    Connectivity().onConnectivityChanged.listen((status) {
      if (status == ConnectivityResult.none) {
        pingTest();
      }
    });

    pingTest();

    isNetConnectedController.sink.add(isNetConnected);

    myStream = isNetConnectedController.stream.asBroadcastStream();
  }

  var oneSec = const Duration(seconds: 3);

  pingTest() {
    print("THis is ping test");
    print(isNetConnected);

    Timer.periodic(oneSec, (Timer timer) async {
      try {
        final response = await InternetAddress.lookup('google.com');

        isNetConnected = response.isNotEmpty;
        myStream = isNetConnectedController.stream.asBroadcastStream();

        print(isNetConnected);
        if (isNetConnected == true) {
          timer.cancel();
        }
      } on SocketException catch (e) {
        print(e);

        isNetConnected = false;
        myStream = isNetConnectedController.stream.asBroadcastStream();

        print(isNetConnected);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Text("Flutter", style: TextStyle(color: Colors.black)),
            Text("News", style: TextStyle(color: Colors.blue))
          ],
        ),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: Container(
        child: Align(
          alignment: Alignment.center,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Text(
                'No Internet Connection',
                style: TextStyle(fontSize: 20),
              ),
              const SizedBox(
                height: 15,
              ),
              // Text(
              //   'Connection status:',
              //   style: TextStyle(fontSize: 20),
              // ),
              // const SizedBox(
              //   height: 15,
              // ),
              Text(
                'Is connection success: ${isNetConnected}',
                style: TextStyle(fontSize: 20),
              ),
              const SizedBox(
                height: 15,
              ),
              ElevatedButton(
                child: Text('Check Network Connection'),
                onPressed: () => pingTest(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
