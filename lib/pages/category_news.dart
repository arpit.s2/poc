import 'dart:async';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:newsapi/model/article_model.dart';
import 'package:newsapi/helper/news.dart';
import 'package:newsapi/pages/article_view.dart';
import 'package:newsapi/pages/home.dart';
// import 'package:newsapi/pages/no_network.dart';
import 'package:flutter_offline/flutter_offline.dart';

class CategoryNews extends StatefulWidget {
  final String category;
  CategoryNews({required this.category});

  @override
  State<CategoryNews> createState() => _CategoryNewsState();
}

class _CategoryNewsState extends State<CategoryNews> {
  List<ArticleModel> articles = <ArticleModel>[];
  bool _loading = true;

  var isNetConnected = true;

  StreamController<bool> isNetConnectedController = StreamController<bool>();

  late Stream myStream;

  void initState() {
    super.initState();
    getCategoryNews();
    pingTest();

    Connectivity().onConnectivityChanged.listen((status) {
      if (status == ConnectivityResult.none) {
        pingTest();
      }
    });

    isNetConnectedController.sink.add(isNetConnected);

    myStream = isNetConnectedController.stream.asBroadcastStream();
  }

  var oneSec = const Duration(seconds: 3);

  pingTest() {
    print("THis is ping test");
    print(isNetConnected);

    Timer.periodic(oneSec, (Timer timer) async {
      try {
        final response = await InternetAddress.lookup('google.com');

        isNetConnected = response.isNotEmpty;
        isNetConnectedController.sink.add(isNetConnected);
        print(isNetConnected);
        if (isNetConnected == true) {
          timer.cancel();
        }
      } on SocketException catch (e) {
        print(e);

        isNetConnected = false;
        isNetConnectedController.sink.add(isNetConnected);
        print(isNetConnected);
      }
    });
  }

  getCategoryNews() async {
    CategoryNewsClass news = CategoryNewsClass();
    await news.getNews(widget.category);
    articles = news.news;
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: isNetConnectedController.stream,
      builder: (context, snapshot) {
        print(snapshot.data);
        return snapshot.data == true
            ? Scaffold(
                appBar: AppBar(
                  backgroundColor: Colors.white,
                  iconTheme: IconThemeData(color: Colors.black),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const <Widget>[
                      Text("Flutter", style: TextStyle(color: Colors.black)),
                      Text("News", style: TextStyle(color: Colors.blue)),
                      Text(" | CatArticles",
                          style: TextStyle(color: Colors.black))
                    ],
                  ),
                  centerTitle: true,
                  elevation: 0.0,
                ),
                body: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: [
                        Container(
                          child: ListView.builder(
                            physics: ScrollPhysics(),
                            itemCount: articles.length,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return BlogTile(
                                imageUrl: articles[index].urlToImage,
                                desc: articles[index].description,
                                title: articles[index].title,
                                url: articles[index].url,
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            : Scaffold(
                appBar: AppBar(
                  backgroundColor: Colors.white,
                  iconTheme: const IconThemeData(color: Colors.black),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const <Widget>[
                      Text("Flutter", style: TextStyle(color: Colors.black)),
                      Text("News", style: TextStyle(color: Colors.blue))
                    ],
                  ),
                  centerTitle: true,
                  elevation: 0.0,
                ),
                body: Container(
                  child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const Text(
                          'No Internet Connection',
                          style: TextStyle(fontSize: 20),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        // Text(
                        //   'Connection status:',
                        //   style: TextStyle(fontSize: 20),
                        // ),
                        // const SizedBox(
                        //   height: 15,
                        // ),
                        Text(
                          'Is connection success: ${isNetConnected}',
                          style: TextStyle(fontSize: 20),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        ElevatedButton(
                          child: Text('Check Network Connection'),
                          onPressed: () => pingTest(),
                        ),
                      ],
                    ),
                  ),
                ),
              );
      },
    );
  }
}

class BlogTile extends StatelessWidget {
  final String imageUrl, title, desc, url;
  BlogTile({
    required this.imageUrl,
    required this.desc,
    required this.title,
    required this.url,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ArticleView(blogUrl: url),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 16),
        child: Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(6),
              child: Image.network(imageUrl),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              title,
              style: const TextStyle(
                fontSize: 18,
                color: Colors.black87,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              desc,
              style: TextStyle(
                color: Colors.black54,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
