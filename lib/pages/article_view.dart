import 'dart:async';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:newsapi/pages/no_network.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ArticleView extends StatefulWidget {
  final String blogUrl;
  ArticleView({required this.blogUrl});

  @override
  State<ArticleView> createState() => _ArticleViewState();
}

class _ArticleViewState extends State<ArticleView> {
  final Completer<WebViewController> _completer =
      Completer<WebViewController>();

  var isNetConnected = true;

  StreamController<bool> isNetConnectedController = StreamController<bool>();

  late Stream myStream;

  void initState() {
    super.initState();
    pingTest();

    Connectivity().onConnectivityChanged.listen((status) {
      if (status == ConnectivityResult.none) {
        pingTest();
      }
    });

    isNetConnectedController.sink.add(isNetConnected);

    myStream = isNetConnectedController.stream.asBroadcastStream();
  }

  var oneSec = const Duration(seconds: 3);

  pingTest() {
    print("THis is ping test");
    print(isNetConnected);

    Timer.periodic(oneSec, (Timer timer) async {
      try {
        final response = await InternetAddress.lookup('google.com');

        isNetConnected = response.isNotEmpty;
        isNetConnectedController.sink.add(isNetConnected);
        print(isNetConnected);
        if (isNetConnected == true) {
          timer.cancel();
        }
      } on SocketException catch (e) {
        print(e);

        isNetConnected = false;
        isNetConnectedController.sink.add(isNetConnected);
        print(isNetConnected);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: isNetConnectedController.stream,
      builder: (context, snapshot) {
        print(snapshot.data);
        return snapshot.data == true
            ? Scaffold(
                appBar: AppBar(
                  backgroundColor: Colors.white,
                  iconTheme: IconThemeData(color: Colors.black),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const <Widget>[
                      Text("Flutter", style: TextStyle(color: Colors.black)),
                      Text("News", style: TextStyle(color: Colors.blue)),
                      Text(" | Articles",
                          style: TextStyle(color: Colors.black)),
                    ],
                  ),
                  centerTitle: true,
                  elevation: 0.0,
                ),
                body: Container(
                  child: WebView(
                    initialUrl: widget.blogUrl,
                    onWebViewCreated: ((WebViewController webViewController) {
                      _completer.complete(webViewController);
                    }),
                  ),
                ),
              )
            : Scaffold(
                appBar: AppBar(
                  backgroundColor: Colors.white,
                  iconTheme: const IconThemeData(color: Colors.black),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const <Widget>[
                      Text("Flutter", style: TextStyle(color: Colors.black)),
                      Text("News", style: TextStyle(color: Colors.blue))
                    ],
                  ),
                  centerTitle: true,
                  elevation: 0.0,
                ),
                body: Container(
                  child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const Text(
                          'No Internet Connection',
                          style: TextStyle(fontSize: 20),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        // Text(
                        //   'Connection status:',
                        //   style: TextStyle(fontSize: 20),
                        // ),
                        // const SizedBox(
                        //   height: 15,
                        // ),
                        Text(
                          'Is connection success: ${isNetConnected}',
                          style: TextStyle(fontSize: 20),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        ElevatedButton(
                          child: Text('Check Network Connection'),
                          onPressed: () => pingTest(),
                        ),
                      ],
                    ),
                  ),
                ),
              );
      },
    );
  }
}
